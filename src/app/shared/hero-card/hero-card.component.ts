import { AfterViewInit, Component, Input, OnInit } from '@angular/core';
import { Hero } from 'src/app/core/models/Hero';

@Component({
  selector: 'app-hero-card',
  templateUrl: './hero-card.component.html',
  styleUrls: ['./hero-card.component.css']
})
export class HeroCardComponent implements OnInit, AfterViewInit {
  @Input()hero!: Hero;
  ngOnInit(): void {

  }
  ngAfterViewInit(): void {

  }
  

}
