import { Component, OnInit } from '@angular/core';
import { Hero } from '../core/models/Hero';
import { HeroesService } from '../core/services/heroes.service';
import BodyResponse from '../core/utils/BodyResponse';
import { HeroResponse } from '../core/utils/HeroResponse';

@Component({
  selector: 'app-marvel',
  templateUrl: './marvel.component.html',
  styleUrls: ['./marvel.component.css']
})
export class MarvelComponent implements OnInit {
  heroes:Hero[] = [];
  isLoading:Boolean;
  publisher:String = "Marvel Comics"
  constructor(private heroesService:HeroesService){
    this.isLoading = true;
  }
  

  ngOnInit():void{
    console.log(this.publisher)
    this.heroesService.getMarvelHeroes(this.publisher).subscribe((response:BodyResponse<HeroResponse>)=>{
        this.heroes = response.data.heroes
        console.log(this.heroes)
        this.isLoading = false
        console.log(response)
    })
  }
}
