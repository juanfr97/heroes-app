export interface Hero {
    superhero:String,
    image:String,
    publisher:String,
    alter_ego:String,
    first_appearance:String,
    characters:String
}