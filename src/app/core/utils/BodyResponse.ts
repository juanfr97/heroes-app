interface BodyResponse<T>{
    data:T,
    statusCode:Number,
    message:String 
}

export default BodyResponse