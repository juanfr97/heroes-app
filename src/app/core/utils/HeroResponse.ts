import { Hero } from "../models/Hero";

export interface HeroResponse {
    total:Number,
    heroes:Hero[]
}