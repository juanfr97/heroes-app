import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'enviroments/environment';
import BodyResponse from '../utils/BodyResponse';
import { HeroResponse } from '../utils/HeroResponse';

@Injectable({
  providedIn: 'root'
})
export class HeroesService {

  constructor(private http:HttpClient) { }

  getMarvelHeroes(publisher:any){
    let params = new HttpParams();
    if(publisher)
      params.append('publisher',publisher)
      return this.http.get<BodyResponse<HeroResponse>>(`${environment.API_URL}/heroes?publisher=${publisher}`)
  }

  getDcHeroes(){
    return this.http.get(`${environment.API_URL}/heroes`)
  }
}
